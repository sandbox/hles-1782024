<?php

/**
 * @file
 * Batch API Code
 */

/**
 * Saves link items in a menu based on available taxonomy terms.
 *
 * @param $terms
 *   A list of taxonomy term objects to build the menu items upon.
 *
 * @param $menu_name
 *   The machine name of the menu in which the menu links should be inserted.
 */
function _taxonomy_menu_localized_save_menu_items_batch($terms, $menu_name) {
  $batch = array(
    'operations' => array(
      array('_taxonomy_menu_localized_save_menu_items_process', array($terms, $menu_name)),
    ),
    'finished' => '_taxonomy_menu_localized_save_link_items_success',
    'title' => t('Rebuilding Taxonomy Menu'),
    'init_message' => t('The menu items have been deleted, and are about to be regenerated.'),
    'progress_message' => t('Import progress: Completed @current of @total stages.'),
    'redirect' => 'admin/structure/taxonomy',
    'error_message' => t('The Taxonomy Menu rebuild process encountered an error.'),
  );
  batch_set($batch);
  batch_process();

  // Rebuild the menu.
  menu_rebuild();
}


/**
 * Processes the batch.
 */
function _taxonomy_menu_localized_save_menu_items_process($terms, $menu_name, &$context) {
  // Menu links are saved 10 by 10.
  _taxonomy_menu_localized_batch_init_context($context, $start, $end, 10);
  // Loop through $terms to process each term.
  foreach ($terms as $term) {
    $mlid = taxonomy_menu_localized_save_menu_item($term, $menu_name);
  }
  // Update the batch context.
  _taxonomy_menu_localized_batch_update_context($context, $end, count($terms), 'Saving menu links');
}

/*
 * Set a message stating the menu has been updated
 */
function _taxonomy_menu_localized_save_menu_items_success() {
  // TODO state menu name here.
  drupal_set_message(t('The menu has been updated.'));
}

/*
 * Initializes the batch context.
 *
 * @param $context array
 *   Batch context array.
 *
 * @param $start int.
 *   The item to start on in this pass.
 *
 * @param $end int.
 *   The end item of this pass
 *
 * @param $items int.
 *   The number of items to process in this pass.
 */
function _taxonomy_menu_localized_batch_init_context(&$context, &$start, &$end, $items) {
  // Initialize sandbox the first time through.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
  }
  $start = $context['sandbox']['progress'];
  $end = $start + $items;
}


/*
 * Update the batch context.
 *
 * @param array $context Batch context array.
 * @param int $end The end point of the most recent pass
 * @param int $total The total number of items to process in this batch
 * @param str $msg Message for the progress bar
 */
function _taxonomy_menu_localized_batch_update_context(&$context, $end, $total, $msg) {
  //Update context array
  if ($end > $total) {
    $context['finished'] = 1;
    return;
  }
  $context['message'] = "{$msg}: {$end} of {$total}";
  $context['sandbox']['progress'] = $end;
  $context['finished'] = $end/$total;
}
