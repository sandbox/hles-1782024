<?php

/**
 * @file
 * Database functions.
 */

/**
 * Inserts a taxonomy term/menu item data association in {taxonomy_menu} table.
 *
 * @param $mlid
 *   the menu link's ID.
 * @param $tid
 *   the term's ID.
 * @param $vid
 *   the vocabulary's ID.
 * @param $langcode
 *   (optional) Defaults to LANGUAGE_NONE.
 *   The language code associated to the menu link's ID.
 */
function _taxonomy_menu_localized_insert_menu_item($mlid, $tid, $vid, $langcode = LANGUAGE_NONE) {
  $fields = array(
    'mlid' => $mlid,
    'tid' => $tid,
    'vid' => $vid,
    'language' => $langcode,
  );
  db_insert('taxonomy_menu')
    ->fields($fields)
    ->execute();
}

/**
 * Returns an mlid based on a taxonomy term ID and vocabulary ID.
 *
 * @param $tid
 *   the term's ID.
 * @param $vid
 *   the vocabulary's ID.
 * @param $langcode
 *   (optional) Defaults to LANGUAGE_NONE.
 *   The language code associated to the menu link's ID.
 *
 * @return
 *   the menu link ID associated to the $tid and $vid. If not found, returns FALSE.
 */
function _taxonomy_menu_localized_get_mlid($tid, $vid, $langcode = LANGUAGE_NONE) {
  // If the language is set to undefined, try to find a menu link ID for the
  // default language anyway. This is to ensure the real source mlid that we
  // want to process is not part of a translation.
  $base_query = db_select('taxonomy_menu', 'tm')
    ->fields('tm', array('mlid',))
    ->condition('tid', $tid)
    ->condition('vid', $vid);

  if ($langcode != LANGUAGE_NONE) {
    // Run the query normally
    $result = $base_query
      ->condition('language', $langcode)
      ->execute()
      ->fetchField();
  }
  else {
    // Run the query normally
    $result = $base_query
    ->condition('language', $langcode)
    ->execute()
    ->fetchField();

    if (!$result) {
      $result = $base_query
      ->condition('language', LANGUAGE_NONE)
      ->execute()
      ->fetchField();
    }
  }

  return $result;
}

/**
 * Return a source mlid.
 * We return an mlid, checking in that order:
 *   - an mlid exists for the default language.
 *   - an mlid exists for an undefined language.
 *   - return FALSE
 *
 * @param $tid
 *   the term's id
 *
 * @param $vid
 *   the term's id
 */
function _taxonomy_menu_localized_get_source_mlid($tid, $vid) {
  $where = array(
    ':tid' => $tid,
    ':vid' => $vid,
    ':language' => $language,
  );
  return db_query('SELECT mlid FROM {taxonomy_menu} WHERE tid = :tid AND vid = :vid AND language = :language', $where)->fetchField();
}

/**
 * Retrieves the term / menu relations for a vocab.
 *
 * @param $vid
 *   the vocabulary's id to delete items from.
 * @return $result
 *   an array (mlid => tid) of items.
 */
function _taxonomy_menu_localized_get_menu_items($vid) {
  $result = db_select('taxonomy_menu', 'tm')
    ->fields('tm', array('mlid', 'tid'))
    ->condition('vid', $vid)
    ->execute()
    ->fetchAllKeyed();

  return $result;
}

 /**
  * Delete all links associated with this vocab from both the taxonomy_menu
  * table and the menu_link table.
  *
  * @param $vid
  *   the vocabulary's id to delete items from.
  */
function _taxonomy_menu_localized_delete_all($vid) {
  $menu_items = _taxonomy_menu_localized_get_menu_items($vid);
  $mlids = array_keys($menu_items);
  if (!empty($mlids)) {
    // Delete all items from the {menu_links} table
    db_delete('menu_links')
      ->condition('mlid', $mlids, 'IN')
      ->execute();
    // Delete all items from the {taxonomy_menu} table
    db_delete('taxonomy_menu')
      ->condition('vid', $vid)
      ->execute();
  }
}

/**
 * Get an array of the tid's related to the node
 *
 * @param $node
 * @return array of tids
 */
function _taxonomy_menu_localized_get_node_terms($node) {
  // Get the taxonomy fields.
  $tids = array();
  $result = db_query("SELECT field_name FROM {field_config} WHERE type = 'taxonomy_term_reference'");
  foreach ($result as $field) {
    $field_name = $field->field_name;

    if (isset($node->$field_name)) {
      $tid_field = $node->$field_name;
      // Loop through all the languages.

      foreach ($tid_field as $tid_field_languages) {
        // Loop through all the tids

        foreach ($tid_field_languages as $tid) {
          $tids[] = $tid['tid'];
        }
      }
    }
  }
  return $tids;
}

/**
 * Get an array of the tid's from the parent
 *
 * @param $tid
 * @return array of tid
 */
function _taxonomy_menu_localized_get_parents($tid) {
  $output = array();
  $result = taxonomy_get_parents($tid);
  foreach ($result as $key => $item) {
    $output[] = $key;
  }
  return $output;
}

/**
  * Delete all rows from {taxomony_menu} associated with this tid
  *
  * @param $vid
  * @param $tid
  */
function _taxonomy_menu_localized_delete_item($vid, $tid) {
  db_delete('taxonomy_menu')
    ->condition('vid', $vid)
    ->condition('tid', $tid)
    ->execute();
}

/**
 * Get all of the tid for a given vid
 *
 * @param $vid
 * @return array of $tid
 */
function _taxonomy_menu_localized_get_terms($vid) {
  $result = db_select('taxonomy_term_data', 'td')
    ->condition('vid', $vid)
    ->fields('td', array('tid'))
    ->execute();
  return $result->fetchAll();
}

/**
 * @TODO Needs Updating since terms are related via node fields
 *
 * used to get the count without children
 *
 * @param $tid
 */
function _taxonomy_menu_localized_term_count($tid) {
  $result = db_select('taxonomy_index', 'tn');
  $result->condition('tid', $tid);
  $result->join('node', 'n', 'n.nid = tn.nid AND n.status = 1');
  $result->addExpression('COUNT(n.nid)', 'term_count');
  $temp = $result->execute();
  $temp = $temp->fetchObject();
  return $temp->term_count;
}

/**
 * Get tid for a given mlid
 *
 * @param $mlid
 * @return $tid
 */
function _taxonomy_menu_localized_get_tid($mlid) {
  $where = array(
    ':mlid' => $mlid,
  );
  return db_query('SELECT tid FROM {taxonomy_menu} WHERE mlid = :mlid', $where)->fetchField();
}

/**
 * Get vid, tid for a given mlid
 *
 * @param $mlid
 * @return array of vid, tid
 */
function _taxonomy_menu_localized_get_item($mlid) {
  $result = db_select('taxonomy_menu', 'tm')
    ->condition('mlid', $mlid, '=')
    ->fields('tm', array('tid', 'vid'))
    ->execute();
  return $result->fetch();
}

/**
 * Get the vocabulary for a tid
 * @param $tid array of tids
 * @return $vid
 */
function _taxonomy_menu_localized_get_vid_by_tid($tids) {
  if ($tids) {
    $result = db_select('term_data')
      ->condition('tid', $tids, 'IN')
      ->fields('term_data', array('vid'))
      ->distinct()
      ->execute();
    $vids = array();
    return $result->fetchAllAssoc('vid');
  }
}
